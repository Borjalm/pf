import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HTML {

	static void enviarPagina(HttpSession session, HttpServletResponse response, String contenido, String titulo, String css, String js)
			throws IOException {
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<meta charset=\"UTF-8\">");
			out.printf("<title>Práctica final DAW</title\n>", titulo);
			out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/base.css\" media=\"screen\" />");
			out.println(" <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\">\r\n" + 
					"    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\r\n" + 
					"    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>\r\n" + 
					"    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\"></script>");
			if (css != null)
				out.printf("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/%s.css\" media=\"screen\" />\n", css);
			if (js != null)
				out.printf("<script type=\"text/javascript\" src=\"js/%s.js\"></script>\n", js);
			out.println("</head>");
			out.println("<body onload=\"load()\">");
			out.println("<header>");
			out.println("	<div class=\"head\">");
			out.printf("		<h2>%s</h2>\n", titulo);
			out.println("		<img src=\"img/inicio.png\" />");
			
			out.println("	</div>");
			out.println("	<nav><div class=\"izda\">");
			if (!titulo.contentEquals("Inicio"))
				out.println("		<span><a class=\"menuitem\" href=\"/pf/\">Inicio</a></span>");
			if (session != null && !titulo.equals("Inicio"))
				out.println("		<span><a class=\"menuitem\" href=\"gestion\">Inicio</a></span>");
			out.println("	</div><div class=\"dcha\">");
			if (session == null) {
				if (!titulo.equals("Inicio de Sesión"))
					out.println("		<span><a class=\"menuitem\" href=\"login\">Iniciar sesión</a></span>");
				out.println("		<span><a class=\"menuitem\" href=\"registro\">Registrarse</a></span>");
			}
			else
				out.printf("		%s (<a href=\"logout\">cerrar sesión</a>)", session.getAttribute("usuario"));
			out.println("	</div></nav>");
			out.println("</header>");			
			out.println("<section class=\"principal\">");
			out.println(contenido);
			out.println("</section>");
			out.println("<footer>");
			out.println("<p>Desarrollado por :<b> Borja Lara Menéndez 2ºDAW </p>");
			out.println("	<div class=\"redes\">");
			out.println("	 <a href=\"https://www.instagram.com/the_coffeest\" target='blank'  />");
			out.println("	<img src=\"img/rss1.png\" />");
			out.println("	</a>");
			out.println("	 <a href=\"https://es-es.facebook.com/thecoffeestaviles\"target='blank'  />");
			out.println("		<img src=\"img/fb.png\" />");
			out.println("	</a>");
			out.println("	</div>");
			out.println("</footer>");
			out.println("</body>");
			out.println("</html>");
		} finally {
			if (out != null)
				out.close();
		}
	}
	
	static void enviarInicio(HttpSession session, HttpServletResponse response) throws IOException {
		HTML.enviarPagina(session, response, aviso("<img src=\"img/logotipodef.png\" /></br><b>Especialistas en café</b>\""), "Inicio", null, null);
	}
	

	
	static String aviso(String mensaje) throws IOException {
		StringBuilder html = new StringBuilder();
		html.append("<div class=\"sinpermiso\">");
		html.append(divError(mensaje));
		html.append("</div>");
		return html.toString();
	}
	
	static void enviarSinPermiso(HttpServletResponse response) throws IOException {
		enviarPagina(null, response, aviso("no tiene permiso para acceder a este recurso"), "Recurso no disponible", null, null);
	}
	
	static String divError(String mensaje) {
		return String.format("<div class=\"error\"><p>%s</p></div>", mensaje);
	}
	

	
	public static void enviarFormUsuario(HttpSession session, HttpServletResponse response, String action, String titulo, String id, String txtSubmitting, String txtSubmit, Estado estado,boolean login) throws IOException {
		StringBuilder html = new StringBuilder();
		
		html.append("	<div class=\"form\">\n");
		if (estado != Estado.OK)
			html.append(divError(estado.toString()));
		html.append(String.format("	<form action=\"%s\" method=\"post\" onsubmit=\"return validar('%s')\">", action, txtSubmitting)); 
		html.append("		<p><label for=\"id\" id=\"idlbl\">Usuario</label></p>\n");
		html.append("		<p><input type=\"text\" class=\"form-control\" id=\"id\" name=\"id\" oninput=\"if (error) limpiar('id')\"");
		if (estado != Estado.OK) {
			if (id != null)
				html.append(String.format(" value=\"%s\"", id));
		}
		html.append(" /></p>\n");
		html.append("		<p><label for=\"password\" id=\"passwordlbl\">Contraseña</label></p>\n");
		html.append("		<p><input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" oninput=\"if (error) limpiar('password')\"/></p>\n");
		if(login==false) {
			html.append("		<p><label for=\"email\" id=\"emaillbl\">Email</label></p>\n");
			html.append("		<p><input type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" oninput=\"if (error) limpiar('email')\"/></p>\n");
		}
		html.append(String.format("		<p class=\"bottom\">"
				+"<button  type=\"submit\" id=\"enviar\" class=\"btn btn-outline-light\" value=\\\"%s\\\">Enviar</button></p>\n", txtSubmit));
		html.append("	</form>\n");
		html.append("	</div>");
		HTML.enviarPagina(session, response, html.toString(), titulo, "frmusuario", "frmusuario");
	}
	
}
